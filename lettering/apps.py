from django.apps import AppConfig


class LetteringConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'lettering'
