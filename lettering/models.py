from django.db import models
from django.forms import model_to_dict

from customers.models import Client


class Newsletter(models.Model):
    send_datetime = models.DateTimeField()
    message_text = models.TextField()
    client_filter = models.JSONField(null=True, blank=True)  # ключ-свойство, значение-фильтр
    end_datetime = models.DateTimeField()

    def full_statistic(self):
        messages = Message.objects.filter(newsletter=self).all()
        messages_list = {i[0]: 0 for i in MessageStatus.choices}
        for i in messages:
            messages_list[i.status] += 1

        result = {
            'send_datetime': self.send_datetime,
            'message_text': self.message_text,
            'client_filter': self.client_filter,
            'end_datetime': self.end_datetime,
            'messages': messages_list
        }
        return result

    def __str__(self):
        return f'{self.message_text[:11]} ...'


class MessageStatus(models.TextChoices):
    CREATED = 'создано'
    SEND = 'отправлено'
    DELIVERED = 'доставлено'
    POSTPONED = 'отложено'
    CANT_BE_DELIVERED = 'не может быть доставлено'
    VOIDED = 'просрочено'


class Message(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    status = models.CharField(choices=MessageStatus.choices)
    newsletter = models.ForeignKey(Newsletter, on_delete=models.CASCADE)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)

    def serialize(self):
        return model_to_dict(self)

    def __str__(self):
        return f'{self.created_at} {self.status}'
