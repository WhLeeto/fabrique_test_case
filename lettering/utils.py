import asyncio
import datetime
import logging
from os import environ

import aiohttp
from rest_framework.exceptions import ValidationError
from django.db.models import Q
from django.utils import timezone

from customers.models import Client
from lettering.models import Message, Newsletter

logger = logging.getLogger(__name__)

fab_token = environ.get('API_TOKEN')


def create_logger_msg(log_type: str, phone: int, status: int = None, resp_text: str = None) -> str:
    """
    Создать сообщение для логгера.
    :param log_type: one of info, warning, error
    :param phone: Параметр из http запроса
    :param status: Параметр из http запроса
    :param resp_text: Параметр из http запроса
    :return: Строка вывода в логгер
    """
    # просто на будующее
    if log_type == 'info':
        return f'Message have been sent to {phone}'
    if log_type == 'warning':
        return
    if log_type == 'error':
        return f'Cant send message to {phone}.\nResp status:{status}\nResp:{resp_text}'
    else:
        return f'Wrong logger func! Check logger!'


async def send_message_to_client(msg: Message, phone: int, text: str) -> bool:
    """
    Отправляет запрос на внешний апи.
    :param msg: Объект сообщения
    :param phone: Телефон получателя
    :param text: Текст сообщения
    :return: bool - результат отправки
    """
    url = f'https://probe.fbrq.cloud/v1/send/{msg.id}'
    headers = {
        'Authorization': fab_token
    }
    body = {
        "id": msg.id,
        "phone": phone,
        "text": text
    }
    async with aiohttp.ClientSession() as session:
        async with session.post(url, headers=headers, json=body) as response:
            if response.status == 200:
                logger.info(create_logger_msg('info', phone))
                return True
            else:
                logger.info(create_logger_msg('error', phone, response.status, response.text()))
                return False


def send_messages(users_to_send: [Client], text, newsletter_id: int) -> None:
    """
    Вызывает ф-ю обращения на внешний апи и меняет статус сообщения в зависимости от результата.
    :param users_to_send: Список пользователей на отправку
    :param text: Текст сообщения
    :param newsletter_id: Id рассылки
    :return: None
    """
    for i in users_to_send:
        new_message = Message.objects.create(
            status='создано',
            newsletter=Newsletter.objects.get(pk=newsletter_id),
            client=i
        )
        result = asyncio.run(send_message_to_client(new_message, i.phone_number, text))
        if result:
            new_message.status = 'доставлено'
        else:
            new_message.status = 'не может быть доставлено'
        new_message.save()


def check_filters(client_filters: list) -> list:
    """
    Проверяет валидность переданных фильтров. Поднимает ошибку в случае не валидности.
    :param client_filters: Список фильтров из view.
    :return: Список фильтров.
    """
    if not client_filters:
        return
    available_filters = [i.name for i in Client._meta.get_fields()]
    current_filters = []
    # todo не учтено что могут отправить список фильтров по ключу
    for key, value in client_filters.items():
        if key not in available_filters:
            raise ValidationError(f'{key} is not valid filter')
        current_filters.append(Q(**{key: value}))
    return current_filters


def create_postponed_messages(users_to_send: [Client], newsletter_id: int) -> None:
    """
    Создает сообщения для отложенной рассылки.
    :param users_to_send: Список клиентов на отправку
    :param newsletter_id: id рассылки
    :return: None
    """
    for client in users_to_send:
        Message.objects.create(
            status='отложено',
            newsletter=Newsletter.objects.get(pk=newsletter_id),
            client=client
        )


def process_new_newsletter(new_newsletter: dict) -> None:
    """
    Основная логика при добавлении новой рассылки. Пытается разослать сообщения или создает отложенные сообщения.
    :param new_newsletter: Поля новой рассылки.
    :return: None
    """
    current_filters = check_filters(new_newsletter.get('client_filter'))
    users_to_send = Client.objects.filter(*current_filters).all()
    send_from = datetime.datetime.fromisoformat(new_newsletter.get('send_datetime'))
    send_before = datetime.datetime.fromisoformat(new_newsletter.get('end_datetime'))
    if send_from < timezone.now() < send_before:
        send_messages(users_to_send, new_newsletter.get('text'), new_newsletter.get('id'))
    if timezone.now() < send_from:
        create_postponed_messages(users_to_send, new_newsletter.get('id'))