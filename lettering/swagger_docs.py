from drf_yasg import openapi

newsletter_post = {
    'description': 'Создать новую рассылку'
}

newsletter_update = {
    'description': 'Обновить рассылку'
}

newsletter_destroy = {
    'description': 'Удалить рассылку и связанные поля. Но я все равно бы лучше сделал деактивацию а не удаление Т_Т.'
}

newsletter_statistics_list = {
    'description': 'Получения общей статистики по созданным рассылкам и количеству отправленных сообщений по ним с '
                   'группировкой по статусам',
    'responses': {
        '200': openapi.Response(
            description='Статистика по рассылкам',
            examples={'application/json': [
                {
                    "send_datetime": "2024-02-10T09:12:49Z",
                    "message_text": "Рассылка косынка",
                    "client_filter": None,
                    "end_datetime": "2024-02-15T09:12:52Z",
                    "messages": {
                        "отправлено": 1,
                        "доставлено": 1,
                        "не может быть доставлено": 1
                    }
                }
            ]}
        ),
        '401': openapi.Response(
            description='Ошибка в запросе',
            examples={'application/json': {
                "detail": "Учетные данные не были предоставлены."
            }}
        )
    }
}

newsletter_statistics_retrive = {
    'description': 'Получения детальной статистики отправленных сообщений по конкретной рассылке',
    'responses': {
        '200': openapi.Response(
            description='Статистика по рассылкам',
            examples={'application/json': [
                {
                    "id": 1,
                    "status": "отправлено",
                    "newsletter": 1,
                    "client": 1
                },
                {
                    "id": 2,
                    "status": "доставлено",
                    "newsletter": 1,
                    "client": 1
                },
                {
                    "id": 3,
                    "status": "не может быть доставлено",
                    "newsletter": 1,
                    "client": 1
                }
            ]}
        ),
        '401': openapi.Response(
            description='Ошибка в запросе',
            examples={'application/json': {
                "detail": "Учетные данные не были предоставлены."
            }}
        )
    }
}
