from django.contrib import admin

from lettering.models import Newsletter, Message


@admin.register(Newsletter)
class NewsletterAdmin(admin.ModelAdmin):
    list_display = ['send_datetime', 'end_datetime']
    list_display_links = ['send_datetime', 'end_datetime']
    search_fields = ['pk', 'send_datetime', 'message_text']


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = ['created_at', 'status', 'client']
    list_display_links = ['created_at', 'status', 'client']
    search_fields = ['created_at', 'status', 'client']
