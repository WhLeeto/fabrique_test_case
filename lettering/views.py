from django.shortcuts import render
from drf_yasg.utils import swagger_auto_schema
from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework.authentication import TokenAuthentication
from rest_framework.views import APIView
from rest_framework.viewsets import mixins, GenericViewSet
from rest_framework.permissions import IsAuthenticated

from lettering.models import Newsletter, Message
from lettering.serializers import NewsletterSerializer, MessageSerializer
from lettering.swagger_docs import newsletter_post, newsletter_statistics_list, newsletter_statistics_retrive, \
    newsletter_update, newsletter_destroy
from lettering.utils import process_new_newsletter


class NewsletterViewSet(mixins.CreateModelMixin,
                        mixins.UpdateModelMixin,
                        mixins.DestroyModelMixin,
                        GenericViewSet):
    permission_classes = [IsAuthenticated]
    serializer_class = NewsletterSerializer
    queryset = Newsletter.objects.all()

    @swagger_auto_schema(tags=['News_letter'],
                         request_body=NewsletterSerializer,
                         operation_description=newsletter_post.get('description'))
    def create(self, request, *args, **kwargs):
        result = super().create(request, *args, **kwargs)
        process_new_newsletter(result.data)
        return result

    @swagger_auto_schema(tags=['News_letter'],
                         request_body=NewsletterSerializer,
                         operation_description=newsletter_update.get('description'))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @swagger_auto_schema(tags=['News_letter'],
                         request_body=NewsletterSerializer,
                         operation_description=newsletter_update.get('description'))
    def partial_update(self, request, *args, **kwargs):
        return super().partial_update(request, *args, **kwargs)

    @swagger_auto_schema(tags=['News_letter'],
                         request_body=NewsletterSerializer,
                         operation_description=newsletter_destroy.get('description'))
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)


class NewsletterStatistic(mixins.ListModelMixin,
                          mixins.RetrieveModelMixin,
                          GenericViewSet):
    permission_classes = [IsAuthenticated]
    queryset = Newsletter.objects.all()
    serializer_class = NewsletterSerializer

    @swagger_auto_schema(tags=['Statistics'],
                         operation_description=newsletter_statistics_list.get('description'),
                         responses=newsletter_statistics_list.get('responses'))
    def list(self, request, *args, **kwargs):
        result = [i.full_statistic() for i in self.queryset]
        return Response(result)

    @swagger_auto_schema(tags=['Statistics'],
                         operation_description=newsletter_statistics_retrive.get('description'),
                         responses=newsletter_statistics_retrive.get('responses'))
    def retrieve(self, request, id, *args, **kwargs):
        messages = Message.objects.filter(newsletter_id=id).all()
        result = [i.serialize() for i in messages]
        return Response(result)


class MessageViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]

    serializer_class = MessageSerializer
    queryset = Message.objects.all()