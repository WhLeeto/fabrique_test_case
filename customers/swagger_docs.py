

customer_post = {
    'description': 'Создать нового клиента',
}

customer_list = {
    'description': 'Список всех клиентов',
}

customer_retrieve = {
    'description': 'Получить клиента по id',
}

customer_update = {
    'description': 'Обновить поля клиента',
}

customer_destroy = {
    'description': 'Удалить клиента. Это полностью удалит клиента и связанные поля. '
                   'Вообще это очень дорогая операция, не проще ли клиента просто деактивировать?',
}