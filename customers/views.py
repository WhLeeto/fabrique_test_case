from django.shortcuts import render
from drf_yasg.utils import swagger_auto_schema
from rest_framework import generics, viewsets
from rest_framework.viewsets import mixins, GenericViewSet
from rest_framework.authentication import TokenAuthentication, SessionAuthentication
from rest_framework.permissions import IsAuthenticated

from customers.models import Client
from customers.serializers import ClientSerializer
from customers.swagger_docs import customer_post, customer_list, customer_retrieve, customer_update, customer_destroy


class CustomerViewSet(mixins.CreateModelMixin,
                      # mixins.RetrieveModelMixin,
                      mixins.UpdateModelMixin,
                      mixins.DestroyModelMixin,
                      # mixins.ListModelMixin,
                      GenericViewSet):
    permission_classes = [IsAuthenticated]
    serializer_class = ClientSerializer
    queryset = Client.objects.all()

    # @swagger_auto_schema(tags=['Customer'],
    #                      operation_description=customer_list.get('description'))
    # def list(self, request, *args, **kwargs):
    #     return super().list(request, *args, **kwargs)

    # @swagger_auto_schema(tags=['Customer'],
    #                      operation_description=customer_retrieve.get('description'))
    # def retrieve(self, request, *args, **kwargs):
    #     return super().retrieve(request, *args, **kwargs)

    @swagger_auto_schema(tags=['Customer'],
                         request_body=ClientSerializer,
                         operation_description=customer_post.get('description'))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @swagger_auto_schema(tags=['Customer'],
                         request_body=ClientSerializer,
                         operation_description=customer_update.get('description'))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @swagger_auto_schema(tags=['Customer'],
                         request_body=ClientSerializer,
                         operation_description=customer_update.get('description'))
    def partial_update(self, request, *args, **kwargs):
        return super().partial_update(request, *args, **kwargs)

    @swagger_auto_schema(tags=['Customer'],
                         request_body=ClientSerializer,
                         operation_description=customer_destroy.get('description'))
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)
