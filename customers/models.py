from django.core.validators import RegexValidator
from django.db import models


class Timezones(models.TextChoices):
    # Сюда можно добавить остальные часовые пояса
    MSK = '3', 'MSK +3'
    UTC = '0', 'UTC +0'
    CET = '1', 'CET +1'
    EST = '-5', 'EST -5'


class Client(models.Model):
    id = models.AutoField(primary_key=True)
    phone_number_regex = RegexValidator(regex=r'^7\d{10}$',
                                        message="Phone number must be entered in the format: '7XXXXXXXXXX'.")
    phone_number = models.CharField(max_length=11, unique=True, validators=[phone_number_regex])
    # todo регулярка для кода оператора. Должен принимать 3 цифры. Должен быть такой же как в phone_number
    operator_code = models.CharField(max_length=3)
    tag = models.CharField(max_length=50, blank=True, null=True)
    timezone = models.CharField(choices=Timezones.choices, default='MSK')

    def __str__(self):
        return f'{self.pk} {self.phone_number}'
