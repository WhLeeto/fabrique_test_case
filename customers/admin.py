from django.contrib import admin

from customers.models import Client


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = ['phone_number', 'operator_code', 'tag', 'timezone']
    list_display_links = ['phone_number', 'operator_code', 'tag', 'timezone']
    search_fields = ['pk', 'phone_number', 'operator_code']
