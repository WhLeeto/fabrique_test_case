from django.urls import path, include, re_path
from rest_framework.routers import DefaultRouter, SimpleRouter

from customers.views import CustomerViewSet
from lettering.views import NewsletterViewSet, NewsletterStatistic

router = SimpleRouter()
router.register('customer', CustomerViewSet)
router.register('newsletter', NewsletterViewSet)

urlpatterns = [
    path('', include(router.urls)),

    path('auth/', include('djoser.urls')),
    re_path(r'auth/', include('djoser.urls.authtoken')),

    path('statistics/', NewsletterStatistic.as_view({'get': 'list'})),
    path('statistics/<int:id>/', NewsletterStatistic.as_view({'get': 'retrieve'})),
]