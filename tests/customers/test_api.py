import pytest

from customers.models import Client
from tests.lettering.test_api import client


@pytest.fixture
def customer_data():
    return {
        "phone_number": "74766371369",
        "operator_code": "str",
        "tag": "string",
        "timezone": "3"
    }


@pytest.fixture
def customer_obj():
    new_customer = Client.objects.create(
        phone_number="74766371369",
        operator_code="str",
        tag="string",
        timezone="3"
    )
    return new_customer


@pytest.mark.django_db
def test_post_customer(client, customer_data):
    count = Client.objects.count()
    response = client.post('/api/v1/customer/', data=customer_data)
    assert response.status_code == 201
    assert Client.objects.count() == count + 1


@pytest.mark.django_db
def test_patch_customer(client, customer_obj):
    patch_data = {
        'tag': 'test!'
    }
    response = client.patch(f'/api/v1/customer/{customer_obj.id}/', data=patch_data)
    assert response.status_code == 200
    data = response.json()
    assert data['tag'] == 'test!'


@pytest.mark.django_db
def test_put_customer(client, customer_obj):
    put_data = {
        'phone_number': '79896543322',
        'operator_code': '989',
        'tag': 'test!'
    }
    response = client.put(f'/api/v1/customer/{customer_obj.id}/', data=put_data)
    assert response.status_code == 200
    data = response.json()
    assert data['tag'] == 'test!'
    assert data['phone_number'] == '79896543322'
    assert data['operator_code'] == '989'


@pytest.mark.django_db
def test_delete_customer(client, customer_obj):
    response = client.delete(f'/api/v1/customer/{customer_obj.id}/')
    assert response.status_code == 204
    assert Client.objects.count() == 0