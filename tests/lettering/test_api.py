import pytest
from rest_framework.test import APIClient

from rest_framework.test import APIClient
from rest_framework.authtoken.models import Token
from django.contrib.auth.models import User

from lettering.models import Newsletter


@pytest.fixture
def client():
    user = User.objects.create_user(username='test_user', password='test_password')
    token = Token.objects.create(user=user)
    client = APIClient()
    client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
    return client


@pytest.fixture
def newsletter_data():
    return {
        "send_datetime": "2024-02-11T12:57:05.437Z",
        "message_text": "string",
        "client_filter": {"tag": "test"},
        "end_datetime": "2024-02-11T12:57:05.437Z"
    }


@pytest.fixture
def newsletter_obj():
    new_obj = Newsletter.objects.create(
        send_datetime="2024-02-11T12:57:05.437Z",
        message_text="string",
        client_filter={"tag": "test"},
        end_datetime="2024-02-11T12:57:05.437Z"
    )
    return new_obj


@pytest.mark.django_db
def test_get_statistics(client):
    response = client.get('/api/v1/statistics/')
    assert response.status_code == 200


@pytest.mark.django_db
def test_get_statistics_by_id(client, newsletter_obj):
    response = client.get(f'/api/v1/statistics/{newsletter_obj.id}/')
    assert response.status_code == 200


@pytest.mark.django_db
def test_create_lettering(client, newsletter_data):
    count = Newsletter.objects.count()
    response = client.post('/api/v1/newsletter/', data=newsletter_data)
    assert response.status_code == 201
    assert Newsletter.objects.count() == count + 1


@pytest.mark.django_db
def test_patch_lettering(client, newsletter_obj):
    new_data = {
        'message_text': 'test!'
    }
    response = client.patch(f'/api/v1/newsletter/{newsletter_obj.id}/', data=new_data)
    assert response.status_code == 200
    data = response.json()
    assert data['message_text'] == 'test!'


@pytest.mark.django_db
def test_put_lettering(client, newsletter_obj):
    new_data = {
        'message_text': 'test!',
        'send_datetime': '2025-02-11',
        'end_datetime': '2026-02-11'
    }
    response = client.put(f'/api/v1/newsletter/{newsletter_obj.id}/', data=new_data)
    assert response.status_code == 200
    data = response.json()
    assert data['message_text'] == 'test!'
    assert data['send_datetime'] == '2025-02-11T00:00:00+03:00'
    assert data['end_datetime'] == '2026-02-11T00:00:00+03:00'


@pytest.mark.django_db
def test_delete_lettering(client, newsletter_obj):
    response = client.delete(f'/api/v1/newsletter/{newsletter_obj.id}/')
    assert response.status_code == 204
    assert Newsletter.objects.count() == 0



