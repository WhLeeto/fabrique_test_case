def check_undelivered_messages():
    """
    Задача для рассылки отложенных или не отправленных сообщений.
    :return:
    """
    import asyncio
    import datetime
    from lettering.models import Message
    from lettering.utils import send_message_to_client
    from django.utils import timezone

    # Пытаемся отправить все не доставленные сообшения
    undelivered_messages = Message.objects.filter(status='не может быть доставлено').all()
    for i in undelivered_messages:
        send_before = datetime.datetime.fromisoformat(i.newsletter.end_datetime)
        if timezone.now() < send_before:
            result = asyncio.run(send_message_to_client(i, i.client.phone_number, i.newsletter.text))
            if result:
                i.status = 'доставлено'
        else:
            i.status = 'просрочено'
        i.save()

    # Пытаемся отправить все отложенные сообщения
    unsend_messages = Message.objects.filter(status='отложено').all()
    for i in undelivered_messages:
        send_from = datetime.datetime.fromisoformat(i.newsletter.send_datetime)
        send_before = datetime.datetime.fromisoformat(i.newsletter.end_datetime)
        if send_from < timezone.now() < send_before:
            result = asyncio.run(send_message_to_client(i, i.client.phone_number, i.newsletter.text))
            if result:
                i.status = 'доставлено'
        elif send_before < timezone.now():
            i.status = 'просрочено'
        i.save()